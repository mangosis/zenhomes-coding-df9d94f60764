# README #

This README helps to compile, test and run the code
##### Note: #####
A SpringBoot Application
The service is working with volatile-in-memory-storage and Mongodb-Storage, by default in application.yml mongodb storage is enabled "mongodb.enable:true". If you do not have mongodb instance available then you can configure this property as 'false' by changing "mongodb.enable:false" after this the system will use RAM based volatile storage only.  

Spring Profiles:
* mon : For Mongodb Storage
* vol : For Volilite in memory storage

-Dspring.profiles.active=[mon | vol]

## Project ##
Electricity Consumption Managemetn Service

### What is this repository for? ###
* Coding challenge

### How do I get set up? ###

* Install Java 8
* Install Maven (Latest)
* Lombok setup please visit [https://projectlombok.org/setup/overview]

### Build, Test and Run ###

* mvn package
* java -jar ec-service-1.0.0-SNAPSHOT.jar -Dspring.profiles.active=[mon | vol]
* Test with Swagger on your browser [http://localhost:9999/swagger-ui.html]
 

### Who do I talk to? ###

* mysialkot@hotmail.com