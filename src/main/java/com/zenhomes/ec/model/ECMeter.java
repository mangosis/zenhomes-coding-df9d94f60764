package com.zenhomes.ec.model;

import java.time.LocalDateTime;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "ec_meter")
public class ECMeter {
	@Id
	@JsonIgnore
	private String id;
	private String counterId;
	private LocalDateTime timestamp;
	private Double amount;

	public ECMeter(String counterId, Double amount) {
		this.counterId = counterId;
		this.amount = amount;
		timestamp = LocalDateTime.now();
	}
}