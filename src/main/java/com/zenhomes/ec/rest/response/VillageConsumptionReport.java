package com.zenhomes.ec.rest.response;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/***
 * 
 * @author Sikandar Ali Awan
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VillageConsumptionReport {
	private List<VillageConsumption> villages = Collections.emptyList();
}
