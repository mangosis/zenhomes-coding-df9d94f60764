package com.zenhomes.ec.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/***
 * 
 * @author Sikandar Ali Awan
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VillageConsumption {
	@JsonProperty("village_name")
	private String villageName;
	private Double consumption;
}
