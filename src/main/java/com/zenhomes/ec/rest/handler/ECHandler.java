package com.zenhomes.ec.rest.handler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.zenhomes.ec.external.ZenhomesExternalsGateway;
import com.zenhomes.ec.rest.request.CounterCallbackRequest;
import com.zenhomes.ec.rest.response.VillageConsumption;
import com.zenhomes.ec.rest.response.VillageConsumptionReport;
import com.zenhomes.ec.service.ECService;

import lombok.SneakyThrows;

@Component
public class ECHandler {

	private final ECService ecService;
	private final ZenhomesExternalsGateway externalsGateway;

	public ECHandler(final ECService ecService, final ZenhomesExternalsGateway externalsGateway) {
		this.ecService = ecService;
		this.externalsGateway = externalsGateway;
	}

	@SneakyThrows
	public ResponseEntity<?> validateAndSave(final CounterCallbackRequest request) {
		if (isValidRequest(request)) {
			ecService.save(request.getCounterId(), request.getAmount());
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
		}
	}

	private boolean isValidRequest(CounterCallbackRequest request) {
		return !(StringUtils.isEmpty(request) || StringUtils.isEmpty(request.getCounterId())
				|| StringUtils.isEmpty(request.getAmount()));
	}

	public ResponseEntity<VillageConsumptionReport> getConsumptionReport(String duration) {
		final LocalDateTime localDateTime = LocalDateTime.now();
		final List<VillageConsumption> consumptions = new ArrayList<VillageConsumption>();
		final Map<String, Double> reportMap = ecService
				.getReport(localDateTime.minusHours(Integer.parseInt(duration.replace("h", ""))));

		reportMap.entrySet().parallelStream().forEach(entity -> {
			final String counterId = entity.getKey();
			externalsGateway.getVillage(counterId).ifPresent(village -> {
				final Double consumption = BigDecimal.valueOf(entity.getValue()).setScale(3, RoundingMode.HALF_UP)
						.doubleValue();
				consumptions.add(new VillageConsumption(village.getName(), consumption));
			});

		});
		return ResponseEntity.ok(new VillageConsumptionReport(consumptions));
	}

}
