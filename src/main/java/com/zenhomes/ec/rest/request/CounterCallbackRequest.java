package com.zenhomes.ec.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CounterCallbackRequest {
	@JsonProperty("counter_id")
	private String counterId;
	private Double amount;

}
