package com.zenhomes.ec.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zenhomes.ec.rest.handler.ECHandler;
import com.zenhomes.ec.rest.request.CounterCallbackRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/ec/v1")
public class ECController {

	private final ECHandler handler;

	public ECController(final ECHandler handler) {
		this.handler = handler;
	}

	@PostMapping(value = "/counter_callback", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> counterCallback(@RequestBody CounterCallbackRequest request) {
		try {
			return handler.validateAndSave(request);
		} catch (Exception e) {
			log.error("Error: /counter_callback {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GetMapping(value = "/consumption_report", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> consumptionReport(String duration) {
		try {
			return handler.getConsumptionReport(duration);
		} catch (Exception e) {
			log.error("Error: /consumption_report {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

}
