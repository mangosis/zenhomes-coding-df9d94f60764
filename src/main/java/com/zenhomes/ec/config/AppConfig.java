package com.zenhomes.ec.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import com.zenhomes.ec.service.ECService;
import com.zenhomes.ec.service.impl.ECServiceMongo;
import com.zenhomes.ec.service.impl.ECServiceVolatile;

@Configuration
public class AppConfig {

	@Value("${mongodb.enable}")
	private Boolean enableMongo;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Autowired
	private ECServiceMongo eCServiceMongo;

	@Autowired
	private ECServiceVolatile eCServiceVolatile;

	@Bean
	@Primary
	public ECService genECService() {
		return enableMongo ? eCServiceMongo : eCServiceVolatile;
	}

}
