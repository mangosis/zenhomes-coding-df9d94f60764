package com.zenhomes.ec.service;

import java.time.LocalDateTime;
import java.util.Map;

import org.springframework.stereotype.Service;

/***
 * 
 * @author Sikandar Ali Awan
 *
 */
@Service
public interface ECService {

	public void save(String counterId, Double amount);

	public Map<String, Double> getReport(final LocalDateTime timeLimit);

}
