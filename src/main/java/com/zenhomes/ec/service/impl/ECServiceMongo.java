package com.zenhomes.ec.service.impl;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import com.zenhomes.ec.model.ECMeter;
import com.zenhomes.ec.service.ECService;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/***
 * 
 * @author Sikandar Ali Awan
 *
 */
@Slf4j
@Service("ecmongo")
public class ECServiceMongo implements ECService {

	@Autowired
	private MongoTemplate template;

	@SneakyThrows
	public void save(String counterId, Double amount) {
		template.save(new ECMeter(counterId, amount));
		log.info("Inserted CounterId: {}, Amount: {}", counterId, amount);
	}

	public Map<String, Double> getReport(final LocalDateTime timeLimit) {
		final Map<String, Double> map = new HashMap<String, Double>();
		GroupOperation amountSum = Aggregation.group("counterId").sum("amount").as("amount");
		Aggregation aggregation = Aggregation
				.newAggregation(Aggregation.match(Criteria.where("timestamp").gte(timeLimit)), amountSum);

		AggregationResults<ECMeter> output = template.aggregate(aggregation, "ec_meter", ECMeter.class);
		output.getMappedResults().forEach(item -> map.putIfAbsent(item.getId(), item.getAmount()));
		return map;
	}

}
