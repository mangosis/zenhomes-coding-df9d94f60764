package com.zenhomes.ec.service.impl;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.zenhomes.ec.model.ECMeter;
import com.zenhomes.ec.service.ECService;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/***
 * 
 * @author Sikandar Ali Awan
 *
 */
@Service("volatile")
@Slf4j
public class ECServiceVolatile implements ECService {

	private static final List<ECMeter> DATA_STORE = new LinkedList<ECMeter>();

	@SneakyThrows
	public void save(String counterId, Double amount) {
		DATA_STORE.add(new ECMeter(counterId, amount));
		log.info("Inserted CounterId: {}, Amount: {}", counterId, amount);
	}

	public Map<String, Double> getReport(final LocalDateTime timeLimit) {
		final Map<String, Double> map = new HashMap<String, Double>();

		DATA_STORE.parallelStream().filter(mc -> mc.getTimestamp().isAfter(timeLimit)).forEach(mc -> {
			map.put(mc.getCounterId(), mc.getAmount() + map.getOrDefault(mc.getCounterId(), 0d));
		});

		return map;
	}

}
