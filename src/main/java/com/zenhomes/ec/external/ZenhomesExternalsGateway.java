package com.zenhomes.ec.external;

import java.util.Optional;

import org.slf4j.helpers.MessageFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ZenhomesExternalsGateway {

	@Value("${zenhomes.external.endpoint-url}")
	private String zenhomesEndpoint;
	private final RestTemplate restTemplate;

	public ZenhomesExternalsGateway(final RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public Optional<Village> getVillage(String counterId) {
		Optional<VillageCounter> villageCounterOptional = getVillageCounter(counterId);
		if (villageCounterOptional.isPresent()) {
			return Optional.of(villageCounterOptional.get().getVillage());
		} else {
			return Optional.empty();
		}
	}

	public Optional<VillageCounter> getVillageCounter(String counterId) {
		try {
			String url = MessageFormatter.format(zenhomesEndpoint, counterId).getMessage();
			VillageCounter zenVillageCounterInfo = restTemplate.getForObject(url, VillageCounter.class);
			return Optional.of(zenVillageCounterInfo);
		} catch (Exception e) {
			log.error("Error while fetching data from zonhomes external endpoint: {}", e);
			return Optional.empty();
		}
	}

}
