package com.zenhomes.ec.external;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Village {
	private String id;
	private String name;
}
