package com.zenhomes.ec;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.Assert;

import com.zenhomes.ec.rest.handler.ECHandler;
import com.zenhomes.ec.rest.request.CounterCallbackRequest;
import com.zenhomes.ec.rest.response.VillageConsumptionReport;

@SpringBootTest
@ActiveProfiles("vol")
public class ECHandlerWithVolatileT {

	@Autowired()
	private ECHandler handler;

	@Test
	public void volatileServiceT() {
		ResponseEntity<?> response = handler.validateAndSave(new CounterCallbackRequest("1", 900d));
		Assert.isTrue(response.getStatusCode() == HttpStatus.OK, "Expected response OK 200");

		ResponseEntity<VillageConsumptionReport> response2 = handler.getConsumptionReport("1h");
		Assert.isTrue(response2.getStatusCode() == HttpStatus.OK, "Expected response OK 200");
		Assert.isTrue(!response2.getBody().getVillages().isEmpty(), "Expecting data");
	}
	
	@Test
	public void getConsumptionReportT() {
		ResponseEntity<VillageConsumptionReport> response = handler.getConsumptionReport("1h");
		Assert.isTrue(response.getStatusCode() == HttpStatus.OK, "Expected response OK 200");
		Assert.isTrue(response.getBody().getVillages().isEmpty(), "Expecting empty list");
	}

}
