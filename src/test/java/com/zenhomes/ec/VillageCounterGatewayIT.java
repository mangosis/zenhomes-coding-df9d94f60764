package com.zenhomes.ec;

import java.util.Objects;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.zenhomes.ec.external.VillageCounter;
import com.zenhomes.ec.external.ZenhomesExternalsGateway;

@SpringBootTest
public class VillageCounterGatewayIT {

	@Autowired
	private ZenhomesExternalsGateway zenhomeCounterGateway;

	@Test
	public void zenhomeGatewayT() {
		Optional<VillageCounter> optional = zenhomeCounterGateway.getVillageCounter("1");
		Assert.isTrue(optional.isPresent(), "VillageCounterInfo is exptected");
		VillageCounter villageCounter = optional.get();
		Assert.isTrue(Objects.equals(villageCounter.getId(), "1"), "Expecting VillageCounter.id: 1");
		Assert.isTrue(StringUtils.isNotBlank(villageCounter.getVillage().getId()), "Expecting village VillageCounter.village.id");
		Assert.isTrue(StringUtils.isNotBlank(villageCounter.getVillage().getName()), "Expecting village VillageCounter.village.name");
	}
	
}
